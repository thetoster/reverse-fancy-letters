# Reverse fancy letters

Package for npm (hopefully) to reverse fancy texts. E.g. : from 🅔🅧🅐🅜🅟🅛🅔 to `example`.

## Site with the most symbols available

<https://fsymbols.com/>

## Order of buisness

- get data (I need to collect them manually I guess)
- store data locally (as object with name of corresponding letter)
- group chars per letter
- check string for that characters
- return corrected string

### Get data

From what I can see, I need whole HTML strings, create divs in some console and filter out famcy stuff that are not as single character.
